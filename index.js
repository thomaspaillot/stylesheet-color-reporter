#!/usr/bin/env node

const fs = require('fs-extra');
const path = require('path');
const program = require('commander');
const analyze = require('./src/analyze');
const report = require('./src/report');

program
  .version('0.0.0', '-v, --version')
  .option('-d, --dir <path>', 'Directory', './src')
  .option('-s, --syntax <extension>', 'Stylesheet syntax', 'scss')
  .option('-t, --target <path>', 'Target directory', './target')
  .action(({ dir, syntax, target }) => {
    analyze(dir, { syntax })
      .then(data => report.render(data))
      .then(html => {
        try {
          fs.copySync(
            path.resolve(__dirname, './src/report/style.css'),
            `${target}/colporter/style.css`
          );
          fs.writeFileSync(`${target}/colporter/index.html`, html);
        } catch (err) {
          throw new Error('Report cannot be saved');
        }
      })
      .catch(err => {
        throw new Error(err);
      });
  });

program.parse(process.argv);
