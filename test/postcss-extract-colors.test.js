const postcss = require('postcss');
const fs = require('fs-extra');
const postcssNested = require('postcss-nested');
const postcssScss = require('postcss-scss');
const postcssExtractColors = require('../src/postcss-extract-colors');

const run = (input, output) =>
  postcss([postcssNested, postcssExtractColors])
    .process(input, { from: './', syntax: postcssScss })
    .then(result => {
      expect(result.analysis).toEqual(output);
    });

describe('postcss plugin', () => {
  it('should analyzes the CSS and expects to see the results', () => {
    const file = fs.readFileSync('./test/fixtures/scss/all/style.scss').toString();

    const expected = {
      color: {
        green: {
          selectors: ['.class', '.another-class.is-state', '.class-2']
        },
        'var(--grey-dark)': {
          selectors: ['.another-class:hover']
        }
      },
      'background-color': {
        '$color-primary': {
          selectors: ['.class']
        }
      },
      fill: {
        blue: {
          selectors: ['.another-class']
        }
      },
      border: {
        'var(--grey)': {
          selectors: ['.another-class']
        }
      },
      'border-bottom-color': {
        'tint(yellow, 20%)': {
          selectors: ['.class-2']
        }
      }
    };

    return run(file, expected);
  });
});
