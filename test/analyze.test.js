const analyze = require('../src/analyze');

describe('analyze module', () => {
  it('should extract values of color properties from one scss file', () => {
    const expected = {
      properties: [
        {
          name: 'color',
          count: 2,
          colors: [
            {
              value: 'green',
              count: 1,
              selectors: ['.class']
            },
            {
              value: 'var(--grey-dark)',
              count: 1,
              selectors: ['.another-class:hover']
            }
          ]
        }
      ]
    };

    return analyze('./test/fixtures/scss/color', { syntax: 'scss' }).then(data => {
      expect(data).toEqual(expected);
    });
  });

  it('should extract values of color and background properties from one scss file', () => {
    const expected = {
      properties: [
        {
          name: 'color',
          count: 2,
          colors: [
            {
              value: 'green',
              count: 1,
              selectors: ['.class']
            },
            {
              value: 'var(--grey-dark)',
              count: 1,
              selectors: ['.another-class:hover']
            }
          ]
        },
        {
          name: 'background-color',
          count: 1,
          colors: [
            {
              value: '#3c3c3c',
              count: 1,
              selectors: ['.class']
            }
          ]
        }
      ]
    };

    return analyze('./test/fixtures/scss/color-background', { syntax: 'scss' }).then(data => {
      expect(data).toEqual(expected);
    });
  });

  it('should extract values of all color related properties from one scss file', () => {
    const expected = {
      properties: [
        {
          name: 'color',
          count: 2,
          colors: [
            {
              value: 'green',
              count: 3,
              selectors: ['.class', '.another-class.is-state', '.class-2']
            },
            {
              value: 'var(--grey-dark)',
              count: 1,
              selectors: ['.another-class:hover']
            }
          ]
        },
        {
          name: 'background-color',
          count: 1,
          colors: [
            {
              value: '$color-primary',
              count: 1,
              selectors: ['.class']
            }
          ]
        },
        {
          name: 'fill',
          count: 1,
          colors: [
            {
              value: 'blue',
              count: 1,
              selectors: ['.another-class']
            }
          ]
        },
        {
          name: 'border',
          count: 1,
          colors: [
            {
              value: 'var(--grey)',
              count: 1,
              selectors: ['.another-class']
            }
          ]
        },
        {
          name: 'border-bottom-color',
          count: 1,
          colors: [
            {
              value: 'tint(yellow, 20%)',
              count: 1,
              selectors: ['.class-2']
            }
          ]
        }
      ]
    };

    return analyze('./test/fixtures/scss/all', { syntax: 'scss' }).then(data => {
      expect(data).toEqual(expected);
    });
  });

  it('should extract values of all color related properties from multiple scss files', () => {
    const expected = {
      properties: [
        {
          name: 'fill',
          count: 1,
          colors: [
            {
              value: 'blue',
              count: 1,
              selectors: ['.another-class']
            }
          ]
        },
        {
          name: 'border',
          count: 1,
          colors: [
            {
              value: 'var(--grey)',
              count: 1,
              selectors: ['.another-class']
            }
          ]
        },
        {
          name: 'color',
          count: 2,
          colors: [
            {
              value: 'var(--grey-dark)',
              count: 1,
              selectors: ['.another-class:hover']
            },
            {
              value: 'green',
              count: 1,
              selectors: ['.class']
            }
          ]
        },
        {
          name: 'background-color',
          count: 1,
          colors: [
            {
              value: '$color-primary',
              count: 1,
              selectors: ['.class']
            }
          ]
        }
      ]
    };

    return analyze('./test/fixtures/scss/files', { syntax: 'scss' }).then(data => {
      expect(data).toEqual(expected);
    });
  });
});
