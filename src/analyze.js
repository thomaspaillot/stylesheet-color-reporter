const dir = require('node-dir');
const fs = require('fs-extra');
const postcss = require('postcss');
const postcssNested = require('postcss-nested');
const postcssScss = require('postcss-scss');
const postcssExtractColors = require('./postcss-extract-colors');

const readFile = file =>
  new Promise(resolve => fs.readFile(file, (err, data) => resolve(data.toString())));
const getFiles = directory => dir.promiseFiles(directory);
const getFilesAsString = files =>
  Promise.all(files.map(file => readFile(file))).then(filesContent =>
    filesContent.reduce((acc, f) => acc + f, '')
  );
const filterScssFiles = (files, extension) =>
  files.filter(file => {
    const fileSplit = file.split('.');
    const fileExt = fileSplit[fileSplit.length - 1];
    return fileExt === extension;
  });

const transformAnalysis = analysis => ({
  properties: Object.entries(analysis).map(([prop, colors]) => ({
    name: prop,
    count: Object.keys(colors).length,
    colors: Object.entries(colors).map(([value, details]) => ({
      value,
      computedValue: details.computedColor,
      count: details.selectors.length,
      selectors: details.selectors
    }))
  }))
});

const analyze = (directory, options) =>
  getFiles(directory)
    .then(files => filterScssFiles(files, options.syntax))
    .then(getFilesAsString)
    .then(css => {
      const syntax = options.syntax === 'scss' ? postcssScss : undefined;
      return postcss([postcssNested, postcssExtractColors])
        .process(css, { from: './', syntax })
        .then(results => results.analysis);
    })
    .then(transformAnalysis);

module.exports = analyze;
