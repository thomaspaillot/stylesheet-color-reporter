const postcss = require('postcss');
const Color = require('color');

const isAColorVariable = color => color.startsWith('var') || color.startsWith('$');
const isAColorTransform = color => color.startsWith('tint') || color.startsWith('shade');
const isNotAColor = color => isAColorVariable(color) || isAColorTransform(color);

const unwrapColorValue = (root, color) => {
  if (isAColorTransform(color)) {
    const percentage = color.match(/\d+%/)[0].replace('%', '');
    const transformedColor = unwrapColorValue(root, color.match(/\$\w+(-\w+)*/)[0]);
    const c = Color(transformedColor);
    return color.startsWith('tint')
      ? c.whiten(percentage / 100).hex()
      : c.blacken(percentage / 100).hex();
  }

  const colorVar = color.startsWith('var')
    ? color.match(/--\w+(-\w+)*/)[0]
    : color
        .match(/\$\w+(-\w+)*/)[0]
        .replace('$', '\\$')
        .concat('$');
  const regex = new RegExp(colorVar, 'g');
  let returnedValue = null;
  root.walkDecls(regex, decl => {
    if (decl.value && isNotAColor(decl.value)) {
      return unwrapColorValue(root, decl.value);
    }

    returnedValue = decl.value;
    return returnedValue;
  });

  return returnedValue;
};

module.exports = postcss.plugin('postcss-extract-colors', () => (root, result) => {
  const analysis = {};

  root.walkDecls(
    /^color$|^background(-color)?$|^fill$|^border((-top|-left|-bottom|-right)|((-\w+)?-color)?)$|^stroke$/,
    decl => {
      let { value } = decl;
      const {
        prop,
        parent: { selector }
      } = decl;

      if (decl.prop.includes('border')) {
        const borderTypes = ['solid', 'dashed', 'dotted'];
        const borderType = borderTypes
          .map(type => ({ type, exist: value.includes(type) }))
          .find(type => type.exist);

        if (borderType) {
          value = value.substring(value.indexOf(borderType.type) + borderType.type.length).trim();
        }
      }
      const sanitizeProp = prop.includes('border') ? 'border' : prop;
      const computedColor = value && isNotAColor(value) ? unwrapColorValue(root, value) : value;

      analysis[sanitizeProp] = analysis[sanitizeProp] ? analysis[sanitizeProp] : {};
      analysis[sanitizeProp][value] = analysis[sanitizeProp][value]
        ? analysis[sanitizeProp][value]
        : {};
      analysis[sanitizeProp][value].selectors = analysis[sanitizeProp][value].selectors
        ? [...analysis[sanitizeProp][value].selectors, selector]
        : [selector];
      analysis[sanitizeProp][value].computedColor = computedColor;
    }
  );

  result.analysis = analysis; // eslint-disable-line no-param-reassign
});
