const fs = require('fs-extra');
const path = require('path');
const hogan = require('hogan.js');

const templateFile = fs.readFileSync(path.resolve(__dirname, './report.mustache')).toString();
const template = hogan.compile(templateFile);

module.exports = template;
